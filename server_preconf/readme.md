##Instalacja systemu
**Do instalacji systemu niezbędny jest nośnik instalacyjny DVD1. DVD2 nie jest potrzebne**

1. Wybierz opcję **instalacji**
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/1.png)

2. Ustaw język systemu na **angielski** (domyślnie) i zmień układ klawiatury na **polski**. **Sprawdz w polu keyboard test poprawność wpisywania.**
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/2.jpg)

3. Dla serwera SMT wybierz opcję rejestracji sieciowej. Dla serwerów zależnych, ustaw URL lokalnego serwera SMT.
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/3.jpg)

4. Nie dodawaj żadnych usług
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/4.jpg)

5. Wybierz opcję **Default system**
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/5.jpg)

6. Partycjonowanie... w zależności przeznaczenia systemu. Do dev/test wystarczy partycjonoowanie standardowe, w przypadku środowisk produkcyjnych zalecane jest utworzenie oddzielnego pliku dysków i przeznaczenie go w całości np na dane bazodanowe - analogicznie jak w przypadku MSSQL
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/6.jpg)

7. Region zgodny z prawdą **Europe/Poland**
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/7.jpg)

8. Użytkownik standardowy, wykorzystywany do logowania do systemu. Z poziomu tego użytkownika dopiero przechodzimy na inne konta
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/8.jpg)

9. Hasło dla root, oczywiście inne niż w kroku 8. Login root'a w sles12sp4 to **root**
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/9.jpg)

10. Ustawienia instalacyjne; względem domyslnych ustawień, interesują nas zakładki **Software**, **Firewall  and SSH** - Upewniamy się, że SSH i firwall jest włączony, oraz jest reguła dla SSH, **Default systemd target**
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/10.jpg)

11. Domyślną powłokę ustawiamy na **text mode**. Przełącznie pomiędzy powłokami **CTRL + ALT + F1-9**. F5 to konsola, F7 Xserver 
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/11.jpg)

12. **Zalecane** Zablokuj instalację trybu graficznego przez odznaczenie wszystkich opcji w kategorii **Graphical Enviroment**. Odznaczaj od góry do dołu.
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/12.jpg)

13. Po zakończonej instalacji, system uruchomi się ponownie. Możemy usunąć nośnik instalacyjny i wybrać opcję **Boot from Hard disk**
![ImgName](https://bitbucket.org/piotr_kruk/patroni-cluster/raw/165eefce0e5060d5c4146b6500f859a75f376b8f/server_preconf/13.jpg)