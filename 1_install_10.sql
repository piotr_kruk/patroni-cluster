--------------------------------------------------------------------------------
-- FILENAME:      1_install_10.sql                                            --
-- VERSION:       1                                                           --
-- DATABASE:      PostgreSQL 10.0 or later                                    --
-- SUMMARY:       Creates database objects necessary for indirect and direct  --
--                synchronization.                                            --
-- INSTRUCTIONS:  Execute script as user 'postgres' or some other database    --
--                superuser.                                                  --
-- DISPLAY:       monospaced font, 80 columns                                 --
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- CREATE SAMPLE DATABASE                                                     --
--------------------------------------------------------------------------------

CREATE DATABASE idm;




--------------------------------------------------------------------------------
-- CREATE DRIVER'S USER ACCOUNT                                               --
--                                                                            --
-- Create the database account used by the driver.  In practice, this account --
-- should be used exclusively by the driver.                                  --
--------------------------------------------------------------------------------

-- NOTE: 
-- \c = change database
\c idm; 
CREATE USER idm PASSWORD 'novell';




--------------------------------------------------------------------------------
-- CREATE SAMPLE DATABASE ACCOUNTS                                            --
--                                                                            --
-- Create the database accounts that own objects referenced by the driver.    --
--------------------------------------------------------------------------------

CREATE USER indirect PASSWORD 'novell';
CREATE USER direct   PASSWORD 'novell';




--------------------------------------------------------------------------------
-- CREATE INDIRECT SYNCHRONIZATION OBJECTS                                    --
--                                                                            --
-- Create intermediate staging tables and functions used to retrieve primary  --
-- key values.                                                                --
--------------------------------------------------------------------------------

-- Create intermediate staging tables.

-- The driver can only synchronizing with tables of a particular structure.
-- Since many existing tables probably may not be structured that way, these 
-- tables are typically used as an intermediate staging area for data housed
-- elsewhere in the database, hence the use of the term indirect to describe 
-- this synchronization model.  Direct synchronization is possible with tables
-- only if they mirror the structure required by the driver.
--
-- When synchronizing with tables, a class is represented as one parent table 
-- and zero or more child tables.  A parent table is defined as one having a 
-- primary key constraint.  A child table is defined as a table without a 
-- primary key constraint that has a foreign key constraint on the parent 
-- table's primary key.  The parent table is used to hold the class's 
-- single-valued attributes and each child table holds the values of a single,
-- multi-valued attribute.

-- NOTE: 
-- CREATE SEQUENCE statements have to occur before a sequence is 
--   referenced.
CREATE SCHEMA indirect 

CREATE SEQUENCE indirect.seq_grp_idg
CREATE SEQUENCE indirect.seq_usr_idu

CREATE TABLE indirect.usr
(
  idu         BIGINT        NOT NULL,    
  fname       VARCHAR(64),      
  lname       CHAR(64),
  pwdminlen   NUMERIC(4),
  pwdexptime  TIMESTAMP,
  disabled    BOOLEAN,
  username    VARCHAR(64),
  loginame    VARCHAR(64),
  photo       BYTEA,
  manager     BIGINT,  
  CONSTRAINT pk_usr_idu PRIMARY KEY (idu),
  CONSTRAINT fk_usr_manager FOREIGN KEY (manager)
    REFERENCES indirect.usr(idu)
    ON DELETE SET NULL
)

CREATE TABLE indirect.usr_phone
(
  idu      BIGINT       NOT NULL,
  phoneno  VARCHAR(64)  NOT NULL,    
  CONSTRAINT fk_phone_idu FOREIGN KEY (idu)
    REFERENCES indirect.usr(idu) ON DELETE CASCADE
)

CREATE TABLE indirect.usr_fax
(
  idu    BIGINT       NOT NULL,
  faxno  VARCHAR(64)  NOT NULL,
  CONSTRAINT fk_fax_idu FOREIGN KEY (idu)
    REFERENCES indirect.usr(idu) ON DELETE CASCADE
)

CREATE TABLE indirect.grp
(
  idg         BIGINT  DEFAULT nextval('indirect.seq_grp_idg')
                      NOT NULL,
  for_insert  BOOLEAN,        
  CONSTRAINT pk_grp_idg PRIMARY KEY (idg)              
)

CREATE TABLE indirect.grp_owner
(
  idg  BIGINT  NOT NULL,
  idu  BIGINT  NOT NULL,
  CONSTRAINT fk_owner_idg FOREIGN KEY (idg)
    REFERENCES indirect.grp(idg) ON DELETE CASCADE,
  CONSTRAINT fk_owner_idu FOREIGN KEY (idu)
    REFERENCES indirect.usr(idu) ON DELETE CASCADE        
)

CREATE TABLE indirect.grp_member
(
  idg  BIGINT  NOT NULL,
  idu  BIGINT  NOT NULL,    
  CONSTRAINT fk_member_idg FOREIGN KEY (idg)
    REFERENCES indirect.grp(idg) ON DELETE CASCADE,        
  CONSTRAINT fk_member_idu FOREIGN KEY (idu)
    REFERENCES indirect.usr(idu) ON DELETE CASCADE
) 

CREATE TABLE indirect.usr_mbr_of
(
  idu  BIGINT  NOT NULL,
  idg  BIGINT  NOT NULL,    
  CONSTRAINT fk_mbr_of_idu FOREIGN KEY (idu)
    REFERENCES indirect.usr(idu) ON DELETE CASCADE,
  CONSTRAINT fk_mbr_of_idg FOREIGN KEY (idg)
    REFERENCES indirect.grp(idg) ON DELETE CASCADE        
) 

CREATE TABLE indirect.event_time
(
  stamp  TIMESTAMP
)
;


-- NOTE:
-- You can use BIGSERIAL or SERIAL instead of explicitly creating sequence 
--   objects and specifying nextval() as the column default.
-- Caching sequence numbers can create large gaps in sequence.

INSERT INTO indirect.event_time(stamp)
  VALUES(CURRENT_TIMESTAMP); -- prime table

CREATE INDEX idx_match_usr    
  ON indirect.usr(lname, fname);
CREATE INDEX idx_phone_idu    
  ON indirect.usr_phone(idu);
CREATE INDEX idx_fax_idu      
  ON indirect.usr_fax(idu);
CREATE INDEX idx_owner_idg   
  ON indirect.grp_owner(idg);
CREATE INDEX idx_owner_idu 
  ON indirect.grp_owner(idu);   
CREATE INDEX idx_member_idg   
  ON indirect.grp_member(idg);
CREATE INDEX idx_member_idu   
  ON indirect.grp_member(idu);
CREATE INDEX idx_mbr_of_idu   
  ON indirect.usr_mbr_of(idu);
CREATE INDEX idx_mbr_of_idg   
  ON indirect.usr_mbr_of(idg);


-- Create functions used for primary key retrieval.
--
-- Functions return a single, scalar return value.
--
-- This step is optional for publication-only drivers.

-- NOTE:
-- currval() and nextval() return a session-local value for the specified table.
-- Use nextval() if you retrieve primary key values before row insertion,
--   currval() otherwise
CREATE FUNCTION indirect.proc_idg() 
  RETURNS BIGINT
AS     
  'SELECT currval(''indirect.seq_grp_idg'');'
LANGUAGE SQL;

CREATE FUNCTION indirect.proc_idu() 
  RETURNS BIGINT
AS     
  'SELECT nextval(''indirect.seq_usr_idu'');'
LANGUAGE SQL;




--------------------------------------------------------------------------------
-- CREATE DIRECT SYNCHRONIZATION OBJECTS                                      --
--                                                                            --
-- Create views.                                                              --
--------------------------------------------------------------------------------


-- When synchronizing with views, each view represents a single class.  
-- Views provide the abstraction mechanism necessary to synchronize directly 
-- with tables of arbitrary structure.
--
-- Each view MUST have one or more columns prefixed with 'pk_'
-- (case-insensitive).  This prefix identifies to the driver which columns can
-- be used to create a unique association value.  The prefixed columns MUST 
-- uniquely identify a single object.
--
-- Views are inherently read-only and, consequently, can only be used for 
-- publication unless augmented by instead-of-rules.  In order to be 
-- updateable and, consequently, subscribable, instead-of-rules MUST be
-- defined for each view and for each type of operation (insert, update, and
-- delete).  Instead-of-rules are only available as of version 7.4.  To install
-- them, run the accompanying install_instead_of_rules.sql script.

CREATE SCHEMA direct

CREATE VIEW direct.view_usr 
(
  pk_idu,
  fname,
  lname,
  pwdminlen,
  pwdexptime,
  disabled,
  photo,
  username,
  loginame,
  fk__idu__manager,
  mv_phoneno,
  mv_faxno,
  fk_mv__idg__mbr_of
)
AS
SELECT a.idu, a.fname, a.lname, a.pwdminlen, a.pwdexptime, a.disabled,
       a.photo, a.username, a.loginame, a.manager, b.phoneno, c.faxno, d.idg
  FROM indirect.usr a LEFT OUTER JOIN indirect.usr_phone b
                        ON (a.idu = b.idu)
                      LEFT OUTER JOIN indirect.usr_fax c
                        ON (a.idu = c.idu)
                      LEFT OUTER JOIN indirect.usr_mbr_of d
                        ON (a.idu = d.idu)

CREATE VIEW direct.view_grp
(
  pk_idg,
  fk_mv__idu__owner,
  fk_mv__idu__mbr
)
AS
SELECT a.idg, b.idu, c.idu
  FROM indirect.grp a LEFT OUTER JOIN indirect.grp_owner b
                        ON (a.idg = b.idg) 
                      LEFT OUTER JOIN indirect.grp_member c
                        ON (a.idg = c.idg) 
;




--------------------------------------------------------------------------------
-- CREATE TRIGGERED PUBLICATION OBJECTS                                       --
--                                                                            --
-- Create publication event log table and the rules that populate it.         --
--                                                                            --
-- This step is optional for subscription-only drivers or when triggerless    --
-- publication is used.                                                       --
--------------------------------------------------------------------------------


-- Create the tables used to store publication events.
--
-- The table and its columns can be named whatever you want.  Only column order
-- and data type are significant.

-- Create a sequence before referencing it.
CREATE SEQUENCE indirect.seq_log_record_id;

CREATE TABLE indirect.indirect_process
(
  record_id    BIGINT       DEFAULT nextval('indirect.seq_log_record_id')
                            NOT NULL,
  table_key    VARCHAR(64)  NOT NULL,
  status       CHAR(1)      DEFAULT 'N'  NOT NULL,
  event_type   SMALLINT     NOT NULL, 
  event_time   TIMESTAMP    NOT NULL,
  perpetrator  VARCHAR(64)  DEFAULT CURRENT_USER,
  table_name   VARCHAR(64)  NOT NULL,
  column_name  VARCHAR(64),
  old_value    VARCHAR(64),
  new_value    VARCHAR(64),

  CONSTRAINT chk_event_type
    CHECK (event_type IN (1, 2, 3, 4, 5, 6, 7, 8)),
  CONSTRAINT chk_status
    CHECK (status     IN ('N', 'S', 'W', 'E', 'F'))
);

-- NOTE:
-- This table is used to store processed records from the event log table
--   (i.e., records with a status value other than 'N').  Its columns  
--   MUST have the same data types as those in the event log table.  Indexes
--   could be added to increase searchability.
CREATE TABLE indirect.indirect_processed
(
  record_id    BIGINT       NOT NULL,
  table_key    VARCHAR(64)  NOT NULL,
  status       CHAR(1)      NOT NULL,
  event_type   SMALLINT     NOT NULL, 
  event_time   TIMESTAMP    NOT NULL,
  perpetrator  VARCHAR(64),
  table_name   VARCHAR(64)  NOT NULL,
  column_name  VARCHAR(64),
  old_value    VARCHAR(64),
  new_value    VARCHAR(64)
);

-- NOTE:  
-- These indexes are optimal for the types of queries that the publisher
--   issues against the event log table.
-- A unique index is created implicitly for unique columns.
ALTER TABLE indirect.indirect_process ADD CONSTRAINT idx_indirectlog_1 UNIQUE(record_id);
CREATE INDEX idx_indirectlog_2 
  ON indirect.indirect_process(event_time, record_id);
CREATE INDEX idx_indirectlog_3 
  ON indirect.indirect_process(status);


-- NOTE:  
-- This procedure consolidates all post-polling statements necessary to
--   cleanup the event log table.
CREATE FUNCTION indirect.proc_indirectlog() 
  RETURNS VOID
AS     
  'DECLARE 
     c CURSOR IS SELECT * FROM indirect.indirect_process
                   WHERE status != ''N'' FOR UPDATE;
     r indirect.indirect_process%ROWTYPE;
   BEGIN
     OPEN c;   
     FETCH c INTO r;
     LOOP
       EXIT WHEN NOT FOUND; 
       INSERT INTO indirect.indirect_processed
       VALUES
       (
         r.record_id,
         r.table_key,
         r.status,
         r.event_type,
         r.event_time,
         r.perpetrator,
         r.table_name,
         r.column_name,
         r.old_value,
         r.new_value
       );
       DELETE FROM indirect.indirect_process
         WHERE record_id = r.record_id;       
       FETCH c INTO r;
     END LOOP;   
     CLOSE c;     
     RETURN;
   END;'   
LANGUAGE plpgsql;

-- NOTE: 
-- Force immediate complication by invoking.
SELECT indirect.proc_indirectlog();


-- Create a sequence before referencing it.
CREATE SEQUENCE direct.seq_log_record_id;

CREATE TABLE direct.direct_process
(
  record_id    BIGINT        DEFAULT nextval('direct.seq_log_record_id')
                             NOT NULL,
  table_key    VARCHAR(64)   NOT NULL,
  status       CHAR(1)       DEFAULT 'N'  NOT NULL,
  event_type   SMALLINT      NOT NULL, 
  event_time   TIMESTAMP     NOT NULL,
  perpetrator  VARCHAR(64)   DEFAULT CURRENT_USER,
  table_name   VARCHAR(64)   NOT NULL,
  column_name  VARCHAR(64),  
  old_value    VARCHAR(64),
  new_value    VARCHAR(64),
    
  CONSTRAINT chk_eventlog_event_type
    CHECK (event_type IN (1, 2, 3, 4, 5, 6, 7, 8)),
  CONSTRAINT chk_eventlog_status
    CHECK (status     IN ('N', 'S', 'W', 'E', 'F'))
);

CREATE TABLE direct.direct_processed
(
  record_id    BIGINT       NOT NULL,
  table_key    VARCHAR(64)  NOT NULL,
  status       CHAR(1)      NOT NULL,
  event_type   SMALLINT     NOT NULL, 
  event_time   TIMESTAMP    NOT NULL,
  perpetrator  VARCHAR(64),
  table_name   VARCHAR(64)  NOT NULL,
  column_name  VARCHAR(64),
  old_value    VARCHAR(64),
  new_value    VARCHAR(64)
);

ALTER TABLE direct.direct_process ADD CONSTRAINT idx_directlog_1 UNIQUE(record_id);
CREATE INDEX idx_directlog_2
  ON direct.direct_process(event_time, record_id);
CREATE INDEX idx_directlog_3 
  ON direct.direct_process(status);
  
CREATE FUNCTION direct.proc_directlog() 
  RETURNS VOID
AS     
  'DECLARE 
     c CURSOR IS SELECT * FROM direct.direct_process
                   WHERE status != ''N'' FOR UPDATE;
     r direct.direct_process%ROWTYPE;
   BEGIN
     OPEN c;   
     FETCH c INTO r;
     LOOP
       EXIT WHEN NOT FOUND; 
       INSERT INTO direct.direct_processed
       VALUES
       (
         r.record_id,
         r.table_key,
         r.status,
         r.event_type,
         r.event_time,
         r.perpetrator,
         r.table_name,
         r.column_name,
         r.old_value,
         r.new_value
       );
       DELETE FROM direct.direct_process
         WHERE record_id = r.record_id;
       FETCH c INTO r;
     END LOOP;   
     CLOSE c;     
     RETURN;
   END;'   
LANGUAGE plpgsql;

SELECT direct.proc_directlog();  
  

-- NOTE:
-- This function exists soley to facilitate future event processing from the 
--   test scripts.  In practice, future event processing would likely be
--   accomplished by direct insertion into the event log table from somewhere
--   other than staging table triggers.
-- COALESCE returns the first NON-NULL result.
CREATE FUNCTION indirect.proc_event_time() 
  RETURNS TIMESTAMP
AS    
  'SELECT
     COALESCE
     (
       stamp,
       CASE WHEN (stamp is NULL)
         THEN LOCALTIMESTAMP 
       ELSE stamp
       END
     ) 
     FROM indirect.event_time;'
LANGUAGE SQL;


-- Create publication rules.
--
-- Publication rules can reside on staging tables (as in this example) or
-- elsewhere.  Regardless of placement, rows inserted into the event log table
-- should use the table/view/column names known to the driver.
--
-- Triggers can also be used, but they have to be written in a procedural 
-- language such as C. 

-- NOTE:  
-- Rules and triggers execute in alphabetical, not declared order.
CREATE RULE rul_usr_i
  AS ON INSERT TO indirect.usr
DO
( 
  --log publication event for each inserted row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idu=' || currval('indirect.seq_usr_idu')),
     5, --insert row (query-back)
     indirect.proc_event_time(),
     'usr'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idu=' || currval('indirect.seq_usr_idu')),
     5, --insert row (query-back)
     indirect.proc_event_time(),
     'view_usr'
  );    
);    

    
CREATE RULE rul_usr_u
  AS ON UPDATE TO indirect.usr
DO
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idu=' || NEW.idu),
    6, --update row (query-back)
    indirect.proc_event_time(),
    'usr'
  );    
 
  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idu=' || NEW.idu),
    6, --update row (query-back)
    indirect.proc_event_time(),
    'view_usr'
  );        
);
    
    
CREATE RULE rul_usr_d
  AS ON DELETE TO indirect.usr
DO 
(
  --log publication event for each deleted row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idu=' || OLD.idu),
    4, --delete row
    indirect.proc_event_time(),
    'usr'
  );        

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idu=' || OLD.idu),
    4, --delete row
    indirect.proc_event_time(),
    'view_usr'
  );            
);
    
    
CREATE RULE rul_usr_fax_i
  AS ON INSERT TO indirect.usr_fax
DO 
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idu=' || NEW.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'usr'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idu=' || NEW.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_usr'
  );    
);
     
        
CREATE RULE rul_usr_fax_d
  AS ON DELETE TO indirect.usr_fax
DO 
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idu=' || OLD.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'usr'
  );            

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES  
  (
    ('pk_idu=' || OLD.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_usr'
  );                
);    
    
    
CREATE RULE rul_usr_phone_i
  AS ON INSERT TO indirect.usr_phone
DO 
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idu=' || NEW.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'usr'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES 
  (
    ('pk_idu=' || NEW.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_usr'
  );    
);    
     
        
CREATE RULE rul_usr_phone_d
  AS ON DELETE TO indirect.usr_phone
DO
( 
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idu=' || OLD.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'usr'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idu=' || OLD.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_usr'
  );    
);

    
CREATE RULE rul_usr_mbr_of_i
    AS ON INSERT TO indirect.usr_mbr_of
DO
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idu=' || NEW.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'usr'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idu=' || NEW.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_usr'
  );    
);    
  
      
CREATE RULE rul_usr_mbr_of_d
  AS ON DELETE TO indirect.usr_mbr_of
DO
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idu=' || OLD.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'usr' 
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idu=' || OLD.idu),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_usr'
  );    
);


CREATE RULE rul_grp_member_i
    AS ON INSERT TO indirect.grp_member
DO
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idg=' || NEW.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'grp'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idg=' || NEW.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_grp'
  );    
);    
  
      
CREATE RULE rul_grp_member_d
  AS ON DELETE TO indirect.grp_member
DO
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idg=' || OLD.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'grp' 
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idg=' || OLD.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_grp'
  );    
);
    
    
CREATE RULE rul_grp_i
  AS ON INSERT TO indirect.grp
DO
(
  --log publication event for each inserted row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idg=' || currval('indirect.seq_grp_idg')),
    5, --insert row (query-back)
    indirect.proc_event_time(),
    'grp'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idg=' || currval('indirect.seq_grp_idg')),
    5, --insert row (query-back)
    indirect.proc_event_time(),
    'view_grp'
  );    
);
   
 
CREATE RULE rul_grp_u
  AS ON UPDATE TO indirect.grp
DO
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idg=' || NEW.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'grp'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idg=' || NEW.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_grp'
  );    
);
    
    
CREATE RULE rul_grp_d
  AS ON DELETE TO indirect.grp
DO
(
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idg=' || OLD.idg),
    4, -- delete
    indirect.proc_event_time(),
    'grp'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idg=' || OLD.idg),
    4, -- delete
    indirect.proc_event_time(),
    'view_grp'
  );    
);
    
    
CREATE RULE rul_grp_owner_i
  AS ON INSERT TO indirect.grp_owner
DO
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idg=' || NEW.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'grp'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idg=' || NEW.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_grp'
  );    
);
     
        
CREATE RULE rul_grp_owner_d
  AS ON DELETE TO indirect.grp_owner
DO
(
  --log publication event for each updated row
  INSERT INTO indirect.indirect_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('idg=' || OLD.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'grp'
  );

  INSERT INTO direct.direct_process
  (
    table_key,
    event_type,
    event_time,
    table_name
  )
  VALUES
  (
    ('pk_idg=' || OLD.idg),
    6, --modify row (query-back)
    indirect.proc_event_time(),
    'view_grp'
  );    
);




--------------------------------------------------------------------------------
-- CREATE INSTEAD-OF-RULES                                                    --
--                                                                            --
-- Create instead-of-rules to enable direct subscription.                     --  
-- Instead-of-rules can validate data, ignore operations, redirect operations --
-- to base tables backing a view, and log publication events.                 --
--                                                                            --
-- This step is optional for publication-only drivers and is not supported by --
-- all database versions.                                                     --
--------------------------------------------------------------------------------


CREATE RULE rul_view_usr_i1
  AS ON INSERT TO direct.view_usr
  WHERE NOT EXISTS(SELECT idu FROM indirect.usr WHERE idu = NEW.pk_idu)
DO INSTEAD
(
  INSERT INTO indirect.usr
  (
    idu,
    fname,
    lname,
    pwdminlen,
    pwdexptime,
    disabled, 
    username,
    photo,
    manager
  )
  VALUES 
  (  
    NEW.pk_idu,
    NEW.fname,
    NEW.lname,
    NEW.pwdminlen,
    NEW.pwdexptime,
    NEW.disabled,
    NEW.username,
    NEW.photo,
    NEW.fk__idu__manager
  );
);


-- NOTE:
-- If you log publication events from instead-of-rules, be sure to log them 
--   before other operations.  If placed last, they are not executed for some
--   reason.
CREATE RULE rul_view_usr_i2_phoneno
  AS ON INSERT TO direct.view_usr
  WHERE (NEW.mv_phoneno IS NOT NULL) AND 
         (NOT EXISTS(SELECT phoneno FROM indirect.usr_phone
                       WHERE idu     = NEW.pk_idu AND
                             phoneno = NEW.mv_phoneno))
DO INSTEAD
(
  INSERT INTO indirect.usr_phone(idu, phoneno)
    VALUES(NEW.pk_idu, NEW.mv_phoneno);
);        


CREATE RULE rul_view_usr_i3_faxno
  AS ON INSERT TO direct.view_usr
  WHERE (NEW.mv_faxno IS NOT NULL) AND
        (NOT EXISTS(SELECT faxno FROM indirect.usr_fax
                      WHERE idu   = NEW.pk_idu AND
                            faxno = NEW.mv_faxno))
DO INSTEAD
(
  INSERT INTO indirect.usr_fax(idu, faxno)
    VALUES(NEW.pk_idu, NEW.mv_faxno);    
);    
        
CREATE RULE rul_view_usr_i4_mbr_of
  AS ON INSERT TO direct.view_usr
  WHERE (NEW.fk_mv__idg__mbr_of IS NOT NULL) AND
        (NOT EXISTS(SELECT idg FROM indirect.usr_mbr_of
                      WHERE idu = NEW.pk_idu AND
                            idg = NEW.fk_mv__idg__mbr_of))
DO INSTEAD
(
  INSERT INTO indirect.usr_mbr_of(idu, idg)
    VALUES(NEW.pk_idu, NEW.fk_mv__idg__mbr_of);
);    


-- NOTE
-- An unconditional or unqualified DO INSTEAD rule has to exist for a view to
--   to be writeable, even if empty.    
CREATE RULE rul_view_usr_i5
    AS ON INSERT TO direct.view_usr
DO INSTEAD
();    


CREATE RULE rul_view_usr_u1_phoneno
  AS ON UPDATE TO direct.view_usr
  WHERE (NEW.mv_phoneno IS NULL) AND
        (OLD.mv_phoneno IS NOT NULL)
DO INSTEAD
(
  DELETE FROM indirect.usr_phone 
    WHERE idu     = NEW.pk_idu AND 
          phoneno = OLD.mv_phoneno;
);        


CREATE RULE rul_view_usr_u2_faxno
  AS ON UPDATE TO direct.view_usr
  WHERE (NEW.mv_faxno IS NULL) AND
        (OLD.mv_faxno IS NOT NULL)
DO INSTEAD
(
  DELETE FROM indirect.usr_fax
    WHERE idu   = NEW.pk_idu AND 
          faxno = OLD.mv_faxno;
);        
    

CREATE RULE rul_view_usr_u3_mbr_of
  AS ON UPDATE TO direct.view_usr
  WHERE (NEW.fk_mv__idg__mbr_of IS NULL) AND
          (OLD.fk_mv__idg__mbr_of IS NOT NULL)
DO INSTEAD
(
  DELETE FROM indirect.usr_mbr_of 
    WHERE idu = NEW.pk_idu AND
          idg = OLD.fk_mv__idg__mbr_of;
);    


CREATE RULE rul_view_usr_u4
  AS ON UPDATE TO direct.view_usr
DO INSTEAD
(
  UPDATE indirect.usr SET
    fname       = NEW.fname, 
    lname       = NEW.lname,
    pwdminlen   = NEW.pwdminlen,
    pwdexptime  = NEW.pwdexptime,
    disabled    = NEW.disabled,
    loginame    = NEW.loginame,
    username    = NEW.username,
    photo       = NEW.photo,
    manager     = NEW.fk__idu__manager
    WHERE idu = NEW.pk_idu;
);    
    

CREATE RULE rul_d_view_usr
  AS ON DELETE TO direct.view_usr
DO INSTEAD
(
    DELETE FROM indirect.usr_phone   WHERE idu  = OLD.pk_idu;
    DELETE FROM indirect.usr_fax     WHERE idu  = OLD.pk_idu;
    DELETE FROM indirect.usr_mbr_of  WHERE idu  = OLD.pk_idu;
    DELETE FROM indirect.grp_owner   WHERE idu  = OLD.pk_idu;
    DELETE FROM indirect.usr         WHERE idu  = OLD.pk_idu;    
);    


CREATE RULE rul_view_grp_i1
  AS ON INSERT TO direct.view_grp
  WHERE NOT EXISTS(SELECT idg FROM indirect.grp WHERE idg = NEW.pk_idg)
DO INSTEAD
(
  INSERT INTO indirect.grp(for_insert) VALUES(NULL);
);    


CREATE RULE rul_view_grp_i2_mbr
  AS ON INSERT TO direct.view_grp
  WHERE (NEW.fk_mv__idu__mbr IS NOT NULL) AND
        (NOT EXISTS(SELECT idu FROM indirect.grp_member 
                      WHERE idg = NEW.pk_idg AND 
                            idu = NEW.fk_mv__idu__mbr))
DO INSTEAD
(
    INSERT INTO indirect.grp_member(idg, idu)
      VALUES(NEW.pk_idg, NEW.fk_mv__idu__mbr);
);

    
CREATE RULE rul_view_grp_i3_owner
  AS ON INSERT TO direct.view_grp
  WHERE (NEW.fk_mv__idu__owner IS NOT NULL) AND
        (NOT EXISTS(SELECT idu FROM indirect.grp_owner 
                      WHERE idu = NEW.fk_mv__idu__owner AND 
                            idg = NEW.pk_idg))
DO INSTEAD
(
  INSERT INTO indirect.grp_owner(idg, idu)
    VALUES(NEW.pk_idg, NEW.fk_mv__idu__owner);
);    


CREATE RULE rul_view_grp_i4
  AS ON INSERT TO direct.view_grp
DO INSTEAD
();    


CREATE RULE rul_view_grp_u1_mbr
  AS ON UPDATE TO direct.view_grp
  WHERE (NEW.fk_mv__idu__mbr IS NULL) AND
        (OLD.fk_mv__idu__mbr IS NOT NULL)
DO INSTEAD
(
  DELETE FROM indirect.grp_member 
    WHERE idg = NEW.pk_idg AND
          idu = OLD.fk_mv__idu__mbr;
);    


CREATE RULE rul_view_grp_u2_owner
  AS ON UPDATE TO direct.view_grp
  WHERE (NEW.fk_mv__idu__owner IS NULL) AND
        (OLD.fk_mv__idu__owner IS NOT NULL)
DO INSTEAD
(
  DELETE FROM indirect.grp_owner
    WHERE idu = OLD.fk_mv__idu__owner AND
          idg = NEW.pk_idg;
);    


CREATE RULE rul_view_grp_u3
    AS ON UPDATE TO direct.view_grp
DO INSTEAD
();


CREATE RULE rul_view_grp_d
  AS ON DELETE TO direct.view_grp
DO INSTEAD
(
  DELETE FROM indirect.grp_owner  WHERE idg = OLD.pk_idg;
  DELETE FROM indirect.usr_mbr_of WHERE idg = OLD.pk_idg;
  DELETE FROM indirect.grp        WHERE idg = OLD.pk_idg;
);




--------------------------------------------------------------------------------
-- GRANT PRIVILEGES                                                           --
--                                                                            --
-- Grant the driver's user account sufficient database privileges to          --
-- facilitate subscription and publication.                                   -- 
--------------------------------------------------------------------------------


-- Grant privileges necessary for indirect/direct subscription.
--
-- In general, the driver must have INSERT, UPDATE, DELETE, AND SELECT
-- privileges on base tables backing views/views, the EXECUTE privilege on 
-- the functions used to retrieve primary keys.

GRANT SELECT, INSERT, UPDATE, DELETE ON indirect.usr        TO idm;
GRANT SELECT, INSERT, UPDATE, DELETE ON indirect.usr_phone  TO idm;
GRANT SELECT, INSERT, UPDATE, DELETE ON indirect.usr_fax    TO idm;
GRANT SELECT, INSERT, UPDATE, DELETE ON indirect.usr_mbr_of TO idm;
GRANT SELECT, INSERT, UPDATE, DELETE ON indirect.grp        TO idm;
GRANT SELECT, INSERT, UPDATE, DELETE ON indirect.grp_owner  TO idm;
GRANT SELECT, INSERT, UPDATE, DELETE ON indirect.grp_member TO idm;

GRANT SELECT, INSERT, UPDATE, DELETE ON direct.view_usr TO idm;
GRANT SELECT, INSERT, UPDATE, DELETE ON direct.view_grp TO idm;

GRANT EXECUTE ON FUNCTION indirect.proc_idu() TO idm;
GRANT EXECUTE ON FUNCTION indirect.proc_idg() TO idm;


-- Grant privileges necessary for user account management.
--
-- Assign system role(s) to the driver's login account to facilitate user
-- account management.

-- NOTE:
-- Granting the SUPERUSER priviliege makes one a database superuser.
-- CREATEUSER has been removed from Postgres 10+ versions.
ALTER USER idm WITH SUPERUSER;


-- Grant privileges necessary for indirect/direct triggered publication.
--
-- In addition to the SELECT privilege on staging tables/views, SELECT 
-- INSERT, UPDATE, and DELETE privileges MUST be granted on the event log
-- table.

GRANT SELECT ON indirect.usr        TO idm;
GRANT SELECT ON indirect.usr_phone  TO idm;
GRANT SELECT ON indirect.usr_fax    TO idm;
GRANT SELECT ON indirect.usr_mbr_of TO idm;
GRANT SELECT ON indirect.grp        TO idm;
GRANT SELECT ON indirect.grp_owner  TO idm;
GRANT SELECT ON indirect.grp_member TO idm;
GRANT SELECT, INSERT, UPDATE, DELETE ON indirect.indirect_process TO idm;

GRANT SELECT ON direct.view_usr TO idm;
GRANT SELECT ON direct.view_grp TO idm;
GRANT SELECT, INSERT, UPDATE, DELETE ON direct.direct_process TO idm;


-- Grant privileges necessary for indirect/direct triggerless publication.
--
-- In general, only the SELECT privilege on staging tables/views is 
-- necessary to facilitate triggerless publication.

GRANT SELECT ON indirect.usr        TO idm;
GRANT SELECT ON indirect.usr_phone  TO idm;
GRANT SELECT ON indirect.usr_fax    TO idm;
GRANT SELECT ON indirect.usr_mbr_of TO idm;
GRANT SELECT ON indirect.grp        TO idm;
GRANT SELECT ON indirect.grp_owner  TO idm;
GRANT SELECT ON indirect.grp_member TO idm;

GRANT SELECT ON direct.view_usr TO idm;
GRANT SELECT ON direct.view_grp TO idm;


-- Grant miscellaneous privileges.
--
-- These privileges are granted to allow deletion of extraneous events
-- that may be generated, depending upon which configuration options
-- were chosen during driver import, to facilitate future event processing
-- in test scripts, to support logging events to multiple event log tables,
-- and to maintain the event log table.

GRANT DELETE  ON indirect.indirect_process            TO idm;
GRANT INSERT  ON indirect.indirect_processed          TO idm;
GRANT EXECUTE ON FUNCTION indirect.proc_event_time()  TO idm;
GRANT EXECUTE ON FUNCTION indirect.proc_indirectlog() TO idm;

GRANT INSERT, DELETE ON direct.direct_process     TO idm;
GRANT INSERT  ON direct.direct_processed          TO idm;
GRANT EXECUTE ON FUNCTION direct.proc_directlog() TO idm 
