## Konfiguracja FireWall
TCP 0/0:5432
TCP 0/0:2379
TCP 0/0:2380
TCP 0/0:8008

## Konfiguracja serwera (sam postgres)

1. wget https://bitbucket.org/piotr_kruk/patroni-cluster/raw/deea20b49a434be25571606442bf7133f1a93a18/postgresql-9.6.5-1-linux-x64.run
2. sudo chmod +x postgresql-9.6.5-1-linux-x64.run
3. sudo ./postgresql-9.6.5-1-linux-x64.run --unattendedmodeui none --mode unattended   --prefix /opt/netiq/common/postgre --datadir /opt/netiq/common/postgre/data --servicename postgresql
4. sudo passwd postgres
5. sudo service postgresql stop
6. su postgres
7. cd /opt/netiq/common/postgre/data/
8. rm pg_hba.conf
9. wget https://bitbucket.org/piotr_kruk/patroni-cluster/raw/deea20b49a434be25571606442bf7133f1a93a18/pg_hba.conf
10. sudo service postgresql start
11.	cd /opt/netiq/common/postgre/bin/
12.	sudo wget https://bitbucket.org/piotr_kruk/patroni-cluster/raw/deea20b49a434be25571606442bf7133f1a93a18/1_install_10.sql
13.	./createdb idm
14.	./createlang plpgsql idm
15.	./psql -d idm postgres
16.	\i 1_install_10.sql
17. \q (wyjscie z psql)
18. sudo yast (dodaj port 5432 do firewall)

---
## Dodatkowa konfiguracja dla klastra
1. sudo service postgresql stop
2. sudo zypper addrepo https://download.opensuse.org/repositories/server:database:postgresql/SLE_12_SP4/server:database:postgresql.repo
3. sudo zypper refresh
4. sudo zypper install python-psycopg2
5. sudo zypper install python3-psycopg2
6. zypper install python-devel gcc python3
7. **root>** curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
8.	**root>** cd ~
9.	**root>** umask 022
10. **root>** python get-pip.py
11. **root>** pip install six
12. **root>** pip install --upgrade setuptools
13. **root>** pip install psycopg2-binary
14. **root>** pip install patroni
15. **root>** pip install python-etcd

---

## Konfiguracja ETCD
1. sudo wget https://github.com/coreos/etcd/releases/download/v3.3.2/etcd-v3.3.2-linux-amd64.tar.gz
2. sudo tar xvf etcd-v3.3.2-linux-amd64.tar.gz
3. cd etcd-v3.3.2-linux-amd64
4. sudo mv etcd etcdctl /usr/local/bin
5. sudo mkdir -p /var/lib/etcd && sudo mkdir -p /etc/etcd
6. sudo groupadd --system etcd
7. sudo useradd -s /sbin/nologin --system -g etcd etcd
8. sudo chown -R etcd:etcd /var/lib/etcd
9. wgraj plik etcd.service do /usr/lib/systemd/system
10. sudo systemctl daemon-reload
11. sudo systemctl enable etcd
12. sudo systemctl start etcd
---

---

### Inicjalizacja bazy - w przygotowaniu

7. su postgres
8. cd /opt/netiq/common/postgre/bin/
9 ./createdb idm
•	./createlang plpgsql idm
•	./psql -d idm postgres
•	Przenieś plik 1_install_10.sql do /opt/netiq/common/postgre/bin
•	\i 1_install_10.sql